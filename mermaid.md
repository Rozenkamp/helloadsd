```mermaid
	graph TD;
      A[Boer Herman]-->|Flevosap| B[Product Owner];
      B -->A;
      B[Product Owner]-->C[Groep 3];
      C-->|Bedrijfsman| D[Anton]
      C-->|Vormer|E[Gallyon];
      C-->|Specialist|F[Nick];
      C-->|Groepsmaker|G[Remco];
      C-->|Plant|H[Ruben];
      D -->I(Taak1);
      E -->J(Taak2);
      F -->K(Taak3);
      G -->L(Taak4);
      H -->M(Taak5);
      I & J & K & L & M -->N[Project];
      N --> A;
```